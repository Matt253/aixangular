var app = angular.module('app', ['ngRoute']).config(
    ['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/accueil', { template: '<h2> Bienvenue sur la E-Boutique de DD</h2><p>Les chaussures de DD</p><hr/>' })
            .when('/produits', { templateUrl: 'vues/produits.html' })
            .when('/produit/:idProduit', { templateUrl: 'vues/produit.html' })
            .otherwise({ redirectTo: '/accueil' });

    }]);